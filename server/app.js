// TODO: 1. Create barebone Web app server
// TODO: 2. Set up client directory to serve static files
// TODO: 3. Handle messages
// TODO: 4. Parse registration sent from client
// TODO: 5. Determine route requested by client and set up the handler for that route

//Load express
var express = require("express");
var bodyParser=require("body-parser");
var path = require("path");


const CLIENT_FOLDER = path.join(__dirname + '/../client');
const NODE_PORT = process.env.NODE_PORT || 3000;

//Create an instance of express application
var app = express();
/* Serve files from public directory
 __dirname is the absolute path of
 the application directory
 */

app.use(express.static(CLIENT_FOLDER));

//to parse the request body(with raw format)
app.use(bodyParser.urlencoded({extended: false}));

app.post("/register", function(req, res, next) {
    console.log('\nInformation submitted to server:')
    console.log('Employee No: ' + req.body.empNo);
    console.log('Firstname: ' +req.body.firstname);
    console.log('Lastname: ' +  req.body.lastname);
    console.log('Gender: ' + req.body.gender);
    console.log('Birthday: ' + req.body.birthday);
    console.log('Hire Date: ' + req.body.hiredate);
    res.status(200).sendFile(path.join(CLIENT_FOLDER + "/thanks.html"));
});


app.use(function (req, res) {
    res.status(404).sendFile(CLIENT_FOLDER + "/messages/404.html");
});

app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(CLIENT_FOLDER + '/messages/501.html'));
});

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});